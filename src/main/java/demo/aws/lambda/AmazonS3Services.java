/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author neeraj
 */
public class AmazonS3Services {
    
       private static AmazonS3Services instance;

    public static AmazonS3Services getInstance() {
        if (instance == null) {
            instance = new AmazonS3Services();
        }
        return instance;
    }
      private final String userPostBucket = "fb-team-attachments";
       AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                .withRegion("ap-south-1")
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("AKIA2W4VA3GE2EGGZUM4", "XSi5ZWxxzreuNIbiZkZ/vR7ZsV/mWFj84nuO2qj3")))
                .build();
      public S3Object UploadProfilePicToS3(String name, String ImageSource, long time) {
        try {
            String[] split = ImageSource.split(";base64,");
            String extension = split[0].split("/")[1];
            String imageByte = split[1];
            String fileName = name + "_" + time + "_PP" + "." + extension;
            String folderName = "user_data";
            return storeProfilePicTos3ByBase64(imageByte, extension, fileName, folderName);
        } catch (IOException ex) {
            Logger.getLogger(AmazonS3Services.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
      
         public S3Object storeProfilePicTos3ByBase64(String imageByte, String extension, String fileName, String folderName) throws IOException {
        String bucketName = userPostBucket + "/" + folderName;
        byte[] imageByteArray = Base64.decodeBase64(imageByte);
        File tempFile = File.createTempFile(fileName, "." + extension);
        try (FileOutputStream fileOutputStream = new FileOutputStream(tempFile)) {
            fileOutputStream.write(imageByteArray);
            PutObjectRequest putRequest = new PutObjectRequest(bucketName, fileName, tempFile);
            s3client.putObject(putRequest);
            S3Object object = s3client.getObject(new GetObjectRequest(bucketName, fileName));
            return object;
        } catch (SdkClientException ex) {
            Logger.getLogger(AmazonS3Services.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
