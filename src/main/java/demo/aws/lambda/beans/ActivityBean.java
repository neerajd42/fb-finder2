/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.beans;

/**
 *
 * @author IESL
 */
public class ActivityBean {
    private String id;
    private String created_by;
    private String created_ts;
    private String table;
    private String table_pk;
    private String group_id;
    private String email;

    public ActivityBean(String created_by, String created_ts, String table, String table_pk) {
        this.created_by = created_by;
        this.created_ts = created_ts;
        this.table = table;
        this.table_pk = table_pk;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the created_by
     */
    public String getCreated_by() {
        return created_by;
    }

    /**
     * @param created_by the created_by to set
     */
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the table
     */
    public String getTable() {
        return table;
    }

    /**
     * @param table the table to set
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * @return the table_pk
     */
    public String getTable_pk() {
        return table_pk;
    }

    /**
     * @param table_pk the table_pk to set
     */
    public void setTable_pk(String table_pk) {
        this.table_pk = table_pk;
    }
}
