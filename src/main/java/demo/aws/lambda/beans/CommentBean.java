/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.beans;

import demo.aws.lambda.constant.Constants;
import static demo.aws.lambda.constant.Constants.checkNull;
import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 *
 * @author IESL
 */
public class CommentBean {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String id, comment, like, activity_id, workspace_id, created_by, created_ts,smiley_count,show_created_ts,username;

    @Override
    public String toString() {
        return toString(this);
    }

    public String getShow_created_ts() {
        return show_created_ts;
    }

    public void setShow_created_ts(String show_created_ts) {
        this.show_created_ts = show_created_ts;
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(this.getClass().getName()).throwing(this.getClass().getName(), "toString(Object THIS)", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the like
     */
    public String getLike() {
        return like;
    }

    /**
     * @param like the like to set
     */
    public void setLike(String like) {
        this.like = like;
    }

    /**
     * @return the activity_id
     */
    public String getActivity_id() {
        return activity_id;
    }

    /**
     * @param activity_id the activity_id to set
     */
    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    /**
     * @return the workspace_id
     */
    public String getWorkspace_id() {
        return workspace_id;
    }

    /**
     * @param workspace_id the workspace_id to set
     */
    public void setWorkspace_id(String workspace_id) {
        this.workspace_id = workspace_id;
    }

    /**
     * @return the created_by
     */
    public String getCreated_by() {
        return created_by;
    }

    /**
     * @param created_by the created_by to set
     */
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    public String getSmiley_count() {
        return smiley_count;
    }

    public void setSmiley_count(String smiley_count) {
        this.smiley_count = smiley_count;
    }

}
