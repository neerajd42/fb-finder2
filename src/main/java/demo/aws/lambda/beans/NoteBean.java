/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.beans;

/**
 *
 * @author IESL
 */
public class NoteBean {

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the created_by
     */
    public String getCreated_by() {
        return created_by;
    }

    /**
     * @param created_by the created_by to set
     */
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
    private String id;
    private String title;
    private String note;
    private String created_ts;
    private String created_by;    
    private String workspace_id;     
    private String file_url;
    private String related_to;
    private String email;
    private String group_id;
    private String user_post;

    public String getUser_post() {
        return user_post;
    }

    public void setUser_post(String user_post) {
        this.user_post = user_post;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }

    public String getRelated_to() {
        return related_to;
    }

    public void setRelated_to(String related_to) {
        this.related_to = related_to;
    }

    /**
     * @return the workspace_id
     */
    public String getWorkspace_id() {
        return workspace_id;
    }

    /**
     * @param workspace_id the workspace_id to set
     */
    public void setWorkspace_id(String workspace_id) {
        this.workspace_id = workspace_id;
    }
}
