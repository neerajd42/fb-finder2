/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.beans;

import demo.aws.lambda.constant.Constants;
import static demo.aws.lambda.constant.Constants.checkNull;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author TOSHIBA R830
 */
public class RecentActivityBean {

    /**
     * @return the show_created_ts
     */
    public String getShow_created_ts() {
        return show_created_ts;
    }

    /**
     * @param show_created_ts the show_created_ts to set
     */
    public void setShow_created_ts(String show_created_ts) {
        this.show_created_ts = show_created_ts;
    }

    /**
     * @return the comment_id
     */
    public String getComment_id() {
        return comment_id;
    }

    /**
     * @param comment_id the comment_id to set
     */
    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    /**
     * @return the commentList
     */
    public List<CommentBean> getCommentList() {
        return commentList;
    }

    /**
     * @param commentList the commentList to set
     */
    public void setCommentList(List<CommentBean> commentList) {
        this.commentList = commentList;
    }

    /**
     * @return the group_id
     */
    public String getGroup_id() {
        return group_id;
    }

    /**
     * @param group_id the group_id to set
     */
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the activity_id
     */
    public String getActivity_id() {
        return activity_id;
    }

    /**
     * @param activity_id the activity_id to set
     */
    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    /**
     * @return the workshop_id
     */
    public String getWorkshop_id() {
        return workshop_id;
    }

    /**
     * @param workshop_id the workshop_id to set
     */
    public void setWorkshop_id(String workshop_id) {
        this.workshop_id = workshop_id;
    }

    public String getCreated_bys() {
        return created_bys;
    }

    public void setCreated_bys(String created_bys) {
        this.created_bys = created_bys;
    }

    private String id, status, title, related_to, tbl_name, created_by,created_bys;
    private String created_ts;
    private String show_created_ts;
    private String group_id;
    private String email;
    private String comment;
    private String priority;
    private String comment_id;
    private String activity_id;
    private String workshop_id;
    private String smileys, smiley_count, comment_count,person_id;
    private List<CommentBean> commentList;
    private String file_url;

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelated_to() {
        return related_to;
    }

    public void setRelated_to(String related_to) {
        this.related_to = related_to;
    }

    public String getTbl_name() {
        return tbl_name;
    }

    public void setTbl_name(String tbl_name) {
        this.tbl_name = tbl_name;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_ts() {
        return created_ts;
    }

    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuilder buffer = new StringBuilder();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(this.getClass().getName()).throwing(this.getClass().getName(), "toString(Object THIS)", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    /**
     * @return the smileys
     */
    public String getSmileys() {
        return smileys;
    }

    /**
     * @param smileys the smileys to set
     */
    public void setSmileys(String smileys) {
        this.smileys = smileys;
    }

    /**
     * @return the smiley_count
     */
    public String getSmiley_count() {
        return smiley_count;
    }

    /**
     * @param smiley_count the smiley_count to set
     */
    public void setSmiley_count(String smiley_count) {
        this.smiley_count = smiley_count;
    }

    /**
     * @return the comment_count
     */
    public String getComment_count() {
        return comment_count;
    }

    /**
     * @param comment_count the comment_count to set
     */
    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getPerson_id() {
        return person_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
    
}
