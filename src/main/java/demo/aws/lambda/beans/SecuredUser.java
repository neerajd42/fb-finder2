/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.beans;

import demo.aws.lambda.constant.Constants;
import static demo.aws.lambda.constant.Constants.checkNull;
import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 *
 * @author TOSHIBA R830
 */
public class SecuredUser {

    /**
     * @return the workspace_id
     */
    public String getWorkspace_id() {
        return workspace_id;
    }

    /**
     * @param workspace_id the workspace_id to set
     */
    public void setWorkspace_id(String workspace_id) {
        this.workspace_id = workspace_id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email_id
     */
    public String getEmail_id() {
        return email_id;
    }

    /**
     * @param email_id the email_id to set
     */
    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the api_key
     */
    public String getApi_key() {
        return api_key;
    }

    /**
     * @param api_key the api_key to set
     */
    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    /**
     * @return the security_access_key
     */
    public String getSecurity_access_key() {
        return security_access_key;
    }

    /**
     * @param security_access_key the security_access_key to set
     */
    public void setSecurity_access_key(String security_access_key) {
        this.security_access_key = security_access_key;
    }
    private String id;
    private String workspace_id;
    private String username;
    private String password;
    private String email_id;
    private String created_ts;
    private String active;
    private String api_key;
    private String security_access_key;

    public SecuredUser() {
    }

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(SecuredUser.class.getName()).throwing(SecuredUser.class.getName(), "toString(Object THIS)", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
