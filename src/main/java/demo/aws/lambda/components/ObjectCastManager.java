/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.components;

import com.google.gson.Gson;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class ObjectCastManager {

    private static ObjectCastManager instance;

    private ObjectCastManager() {
    }

    public static ObjectCastManager getInstance() {
        if (instance == null) {
            instance = new ObjectCastManager();
        }
        return instance;
    }

    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException {
        Gson gson = new Gson();
        T json = gson.fromJson(jsonInString, clazz);
        return json;
    }

    public <T> T jSONcast(Class<T> clazz, Map map) throws IOException {
        Gson gson = new Gson();
        String toJson = gson.toJson(map);
        return jSONcast(clazz, toJson);
    }

    public <T> T jSONcast(Class<T> clazz, Object obj) throws IOException, InstantiationException, IllegalAccessException {
        if (obj instanceof Map) {
            return jSONcast(clazz, (Map) obj);
        } else if (obj instanceof String) {
            return jSONcast(clazz, (String) obj);
        } else {
            return clazz.newInstance();
        }
    }

    public List<Map> resultSetCasting(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        String[] columnArray = new String[columnCount];
        for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnLabel(i);
            columnArray[i-1] = columnName;
        }
        List<Map> mapList = null;
        Map map = null;
        while (rs.next()) {
            if (mapList == null) {
                mapList = new LinkedList<>();
            }
            map = new HashMap();
            for (String columnName : columnArray) {
                map.put(columnName, rs.getString(columnName));
            }
            mapList.add(map);
        }
        return mapList;
    }
}
