/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author IESL
 */
public class Common {

    private static final String DATE_FORMAT3 = "yyyy-MM-dd";
    private static final String DISPLAY_FORMAT2 = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat format3 = new SimpleDateFormat(DATE_FORMAT3);
    public static final SimpleDateFormat format2 = new SimpleDateFormat(DISPLAY_FORMAT2);

    public Date today() {
        return DateTimeServiceImpl.current();
    }

    public String format3(Date date) {
        if (date == null) {
            return "";
        } else {
            return format3.format(date);
        }
    }

    public String format2(Date date) {
        if (date == null) {
            return "";
        } else {
            return format2.format(date);
        }
    }

    public String getConvertedDate(String month) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("January", "01");
        hashMap.put("February", "02");
        hashMap.put("March", "03");
        hashMap.put("April", "04");
        hashMap.put("May", "05");
        hashMap.put("June", "06");
        hashMap.put("July", "07");
        hashMap.put("August", "08");
        hashMap.put("September", "09");
        hashMap.put("October", "10");
        hashMap.put("November", "11");
        hashMap.put("December", "00");
        String dateString = new SimpleDateFormat("YYYY/MM/dd").format(new Date());
        String[] date = dateString.split("/");
        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(date[0]), Integer.parseInt(hashMap.get(month)), Integer.parseInt(date[2]));
        Integer mm = (cal.get(cal.MONTH) == 0 ? 12 : cal.get(cal.MONTH));
        String finalDate = cal.get(cal.YEAR) + "" + putZero(String.valueOf(mm)) + "" + putZero(String.valueOf(cal.get(cal.DAY_OF_MONTH)));
        return finalDate;
    }

    protected String[] getBankSplit(String accountNumber) {
        return accountNumber.split("-");
    }

    private String putZero(String val) {
        if (val.length() < 2) {
            return "0" + val;
        } else {
            return val;
        }
    }

    public String parseDate(String date) {
        if (date == null) {
//            return new Date();
            return today().toString();
        }
        try {
            return format3.parse(date).toString();
        } catch (ParseException ex) {
            return null;
        }
    }
}
