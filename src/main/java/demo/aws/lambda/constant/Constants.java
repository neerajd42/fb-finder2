package demo.aws.lambda.constant;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract interface Constants {

    final static String httpMethodGet = "GET";
    final static String httpMethodPost = "POST";
    final static String httpMethodPut = "PUT";
    final static String httpMethodDelete = "Delete";

    static String CODE_NEW_LINE = "\n";

    public static String toString(Object THIS)
            throws IllegalArgumentException, IllegalAccessException {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];

            String fieldName = field.getName();
            Object fieldValue = field.get(THIS);
            buffer.append("\"").append(fieldName).append("\"");
            buffer.append(":");
            buffer.append(checkNull(new Object[]{fieldValue}));
            if (i != declaredFields.length - 1) {
                buffer.append(", \n");
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public static String checkNull(Object... oArr) {
        if (oArr != null) {
            Object o = oArr[0];
            Boolean toObject = Boolean.valueOf(false);
            if ((oArr.length > 1) && (((Boolean) oArr[1]).booleanValue())) {
                toObject = Boolean.valueOf(true);
            }
            if ((o instanceof List)) {
                List list = (List) o;
                String objects = "";
                for (int i = 0; i < list.size(); i++) {
                    Object obj = list.get(i);
                    String toString = null;
                    if ((toObject.booleanValue()) && ((obj instanceof Constants))) {
                        toString = ((Constants) obj).toString();
                    } else {
                        toString = obj.toString();
                    }
                    objects = objects + ((i > 0) && (i != list.size()) ? "," : "") + toString;
                }
                return "[" + objects + "]";
            }
            if ((o instanceof Map)) {
                Map map = (Map) o;
                String objects = "";
                List<Map.Entry> list = new ArrayList(map.entrySet());
                for (int i = 0; i < list.size(); i++) {
                    Map.Entry entry = (Map.Entry) list.get(i);
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    String toString = null;
                    if ((toObject.booleanValue()) && ((value instanceof Constants))) {
                        toString = key + ":" + ((Constants) value).toString();
                    } else {
                        toString = "\"" + key + "\":" + value.toString();
                    }
                    objects = objects + ((i > 0) && (i != list.size()) ? "," : "") + toString;
                }
                return "{" + objects + "}";
            }
            if ((o instanceof Double)) {
                return "" + o + "";
            }
            if ((o instanceof Long)) {
                return "" + o + "";
            }
            if ((o instanceof Boolean)) {
                return "" + o + "";
            }
            if ((o instanceof Date)) {
                return "'" + o.toString() + "'";
            }
            if ((o instanceof String)) {
                String val = (String) o;
                if (val.equalsIgnoreCase("null")) {
                    return null;
                }
                return ("\"" + val + "\"").trim();
            }
            return null;
        }
        return null;
    }
}
