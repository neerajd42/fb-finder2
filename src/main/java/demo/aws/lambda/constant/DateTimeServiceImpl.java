/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.constant;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Maninderjit Singh
 */
final public class DateTimeServiceImpl {

    final public static ZoneId TOZONEID = ZoneId.of("Europe/London");

    public static Date current() {
        ZonedDateTime zdt = Instant.now().atZone(TOZONEID);
        LocalDateTime ldt = zdt.toLocalDateTime();
        Date now = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        return now;
    }

    public static Calendar calendar() {
        Calendar instance = Calendar.getInstance();
        TimeZone timeZone = TimeZone.getTimeZone(TOZONEID);
        instance.setTimeZone(timeZone);
        return instance;
    }

   
    public Date today0000() {
        Calendar calendar = calendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        return date;
    }

 
    public Date today1200() {
        Calendar calendar = calendar();
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        return date;
    }

    public Date now() {
        return current();
    }

    public long timeDate() {
        return now().getTime();
    }

}
