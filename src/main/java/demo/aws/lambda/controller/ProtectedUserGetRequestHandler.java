package demo.aws.lambda.controller;

import demo.aws.lambda.repository.PostRepository;
import java.util.HashMap;

public class ProtectedUserGetRequestHandler {

    private ProtectedUserGetRequestHandler() {
    }

    private static ProtectedUserGetRequestHandler instance;

    public static ProtectedUserGetRequestHandler getInstance() {
        if (instance == null) {
            instance = new ProtectedUserGetRequestHandler();
        }
        return instance;
    }

    public void doGetMethod(HashMap request, HashMap response) {
       
                PostRepository.getInstance().fetchPosts(null, response);
           
             
        }
    }

