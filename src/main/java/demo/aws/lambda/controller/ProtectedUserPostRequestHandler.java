package demo.aws.lambda.controller;

import com.amazonaws.services.s3.model.S3Object;
import demo.aws.lambda.AmazonS3Services;
import demo.aws.lambda.beans.LoginBean;
import demo.aws.lambda.beans.NoteBean;
import demo.aws.lambda.beans.RecentActivityBean;
import demo.aws.lambda.repository.LikeCommentRepository;
import demo.aws.lambda.repository.PostRepository;
import demo.aws.lambda.repository.ProtectedUserRepository;
import java.util.Date;
import java.util.HashMap;

public class ProtectedUserPostRequestHandler {

    private ProtectedUserPostRequestHandler() {
    }

    private static ProtectedUserPostRequestHandler instance;

    public static ProtectedUserPostRequestHandler getInstance() {
        if (instance == null) {
            instance = new ProtectedUserPostRequestHandler();
        }
        return instance;
    }

    public void dopostsave(NoteBean bean, HashMap response) {
        if (bean.getFile_url() != null && !bean.getFile_url().isEmpty() && bean.getFile_url().contains(";base64")) {
            S3Object s3Object = AmazonS3Services.getInstance().UploadProfilePicToS3("User", bean.getFile_url(), new Date().getTime());
            String imageUrl = s3Object.getObjectContent().getHttpRequest().getURI().toString();
            System.out.println("imageUrl" + imageUrl);
            bean.setFile_url(imageUrl);
        }
        PostRepository.getInstance().newPost(bean, response);

    }

    public void commentsave(RecentActivityBean bean, HashMap response) {

        LikeCommentRepository.getInstance().newComment(bean, response);

    }
    public void saveRegisteruser(LoginBean bean, HashMap response) {

        ProtectedUserRepository.getInstance().saveRegister(bean, response);

    }

    public void commentlike(RecentActivityBean bean, HashMap response) {
        if (bean.getComment_id() != null && !bean.getComment_id().isEmpty()) {
            LikeCommentRepository.getInstance().updateCommentSmiley(bean, response);
        } else {
            LikeCommentRepository.getInstance().updateSmiley(bean, response);
        }

    }

}
