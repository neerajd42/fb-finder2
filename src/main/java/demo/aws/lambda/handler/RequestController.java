package demo.aws.lambda.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import demo.aws.lambda.beans.LoginBean;
import demo.aws.lambda.beans.NoteBean;
import demo.aws.lambda.beans.RecentActivityBean;
import demo.aws.lambda.components.ObjectCastManager;
import demo.aws.lambda.constant.Common;
import demo.aws.lambda.constant.Constants;
import demo.aws.lambda.controller.ProtectedUserGetRequestHandler;
import demo.aws.lambda.controller.ProtectedUserPostRequestHandler;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestController implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private Gson gson = new Gson();
    private Common common = new Common();

    public RequestController() {
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent requestEvent, Context context) {
        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
        LinkedHashMap payload = new LinkedHashMap();
        LinkedHashMap header = new LinkedHashMap();
        payload.put("request", requestEvent);
        String today = common.format3(common.today());
        try {
            switch (requestEvent.getRequestContext().getResourcePath()) {
                case "/socialapp": {
                    if (requestEvent.getRequestContext().getHttpMethod().equalsIgnoreCase(Constants.httpMethodGet)) {
                        ProtectedUserGetRequestHandler.getInstance().doGetMethod(null, payload);
                    } else {
                        String body = requestEvent.getBody();
                        NoteBean bean = ObjectCastManager.getInstance().jSONcast(NoteBean.class, body);
                        bean.setCreated_ts(today);
                        ProtectedUserPostRequestHandler.getInstance().dopostsave(bean, payload);
                    }
                    responseEvent.setStatusCode(200);
                }
                break;
                case "/socialapp/comment": {
                    String body = requestEvent.getBody();
                    RecentActivityBean bean = ObjectCastManager.getInstance().jSONcast(RecentActivityBean.class, body);
                    bean.setCreated_ts(today);
                    ProtectedUserPostRequestHandler.getInstance().commentsave(bean, payload);
                    responseEvent.setStatusCode(200);
                }
                case "/register": {
                    String body = requestEvent.getBody();
                    LoginBean bean = ObjectCastManager.getInstance().jSONcast(LoginBean.class, body);
                    bean.setCreated_ts(today);
                    payload.remove("request");
                    ProtectedUserPostRequestHandler.getInstance().saveRegisteruser(bean, payload);
                    responseEvent.setStatusCode(200);
                }
                break;
                case "/socialapp/like": {
                    Map<String, String> headers = requestEvent.getQueryStringParameters();
                    RecentActivityBean bean = new RecentActivityBean();
                    bean.setActivity_id(headers.get("activity_id"));
                    bean.setCreated_by(headers.get("created_by"));
                    bean.setComment_id(headers.get("comment_id"));
                    bean.setCreated_ts(today);
                    ProtectedUserPostRequestHandler.getInstance().commentlike(bean, payload);
                    responseEvent.setStatusCode(200);
                }
                break;
                case "/beneficiary/actual-asset-allocation": {
                    String UserId = requestEvent.getPathParameters().get("id");
                    responseEvent.setStatusCode(200);
                }
                break;
                default:
                    responseEvent.setStatusCode(400);
                    payload.put("default", "default");
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(RequestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        header.put("Access-Control-Allow-Headers", "Content-Type");
        header.put("Access-Control-Allow-Origin", "*");
        header.put("Access-Control-Allow-Methods", "OPTIONS,GET,POST");
        responseEvent.setHeaders(header);
        responseEvent.setBody(gson.toJson(payload));
        return responseEvent;
    }
}
