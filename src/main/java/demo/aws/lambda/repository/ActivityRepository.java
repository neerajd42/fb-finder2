package demo.aws.lambda.repository;

import demo.aws.lambda.beans.ActivityBean;
import demo.aws.lambda.components.ObjectCastManager;
import demo.aws.lambda.constant.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ActivityRepository {

    private static ActivityRepository instance;

    public static ActivityRepository getInstance() {
        if (instance == null) {
            instance = new ActivityRepository();
        }
        return instance;
    }

    public void newActivity(Connection con, ActivityBean bean, HashMap response) throws SQLException {
        String sql = " INSERT INTO `save_activity`\n"
                + "(`created_by`,\n" + "`created_ts`,\n" + "`table`,\n" + "`table_pk`,\n" + "`active`)\n"
                + "VALUES\n"
                + "(?,\n" + "?,\n" + "?,\n" + "?,\n" + "?); ";
        try (PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, bean.getCreated_by());
            preparedStatement.setString(2, bean.getCreated_ts());
            preparedStatement.setString(3, bean.getTable());
            preparedStatement.setString(4, bean.getTable_pk());
            preparedStatement.setString(5, "Y");
            preparedStatement.executeUpdate();
           
        }
        
    }

    public void fetchActivities(HashMap request, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            List<Map> activityList = fetchActivities(con, request, response);
            response.put("activityList", activityList);
            response.put("message", "You have requested for recent activities. Now you have these recent activities.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("activityList", null);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(ActivityRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ActivityRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public List<Map> fetchComments(Connection con, String activity_id, String limit) throws SQLException {
        String sql = "  select LC.id, LC.comment, LC.like, LC.created_by, LC.created_ts, LM.username username from like_comment LC\n"
                + " inner join login_master LM on(LM.id = LC.created_by)\n"
                + " where LC.comment <> ''";
        if (activity_id != null && !activity_id.isEmpty()) {
            sql += " and LC.activity_id = " + activity_id;
        }
        sql += "  order by LC.id desc ";
        if (limit != null) {
            sql += " limit " + limit;
        }
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        List<Map> list = ObjectCastManager.getInstance().resultSetCasting(rs);
        return list;
    }

    public List<Map> fetchActivities(Connection con, HashMap request, HashMap response) throws SQLException {
        String workspace_id = (String) request.get("workspace_id");
        String created_by = (String) request.get("created_by");
        String limit = (String) request.get("limit");
        String sql = " SELECT activity_id, created_ts, `table`, table_pk, created_by, group_id,sum(case when `like` is not null and `like` and comment_id is null then 1 else 0 end) as smiley_count, group_concat(distinct case when like_created_by is not null and `like` is not null and like_created_by  <> '' then like_created_by else null end) as created_bys, group_concat(distinct case when `like` is not null and `like` <> '' then `like` else null end) as smileys, sum(case when `comment` is not null and `comment` <> '' and comment_id is null then 1 else 0 end) as comment_count FROM\n"
                + "(\n"
                + "SELECT SA.id activity_id, AG.group_id, SA.table, SA.table_pk, SA.active, LC.comment, LC.like, LC.comment_id, LC.created_by like_created_by, SA.created_by, LM.workspace_id, SA.created_ts FROM save_activity SA\n"
                + "inner join login_master LM on(LM.id = SA.created_by)\n"
                + "inner join activity_group AG on (AG.activity_id = SA.id)\n"
                + "inner join group_users GU on (AG.group_id = GU.group_id and GU.user_id = " + created_by + " and GU.active='Y')\n"
                + "left join like_comment LC on(LC.activity_id = SA.id)\n"
                + "where LM.workspace_id = " + workspace_id + " and SA.active = 'Y'\n"
                + "union all\n"
                + "SELECT SA.id activity_id, AG.group_id, SA.table, SA.table_pk, SA.active, LC.comment, LC.like, LC.comment_id, LC.created_by like_created_by, SA.created_by, LM.workspace_id, SA.created_ts FROM save_activity SA\n"
                + "inner join login_master LM on(LM.id = SA.created_by)\n"
                + "inner join activity_group AG on (AG.activity_id = SA.id)\n"
                + "left join like_comment LC on(LC.activity_id = SA.id)\n"
                + "where LM.workspace_id = " + workspace_id + " and AG.group_id = 0 and SA.active = 'Y') SA\n"
                + "group by SA.activity_id\n"
                + "order by SA.activity_id desc \n";
        if (limit != null) {
            sql += " limit " + limit;
        } else {
            sql += " limit 5";
        }
        System.out.println("+++++++++++++outerSQL  above++++++++++" + sql);
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        List<Map> list = ObjectCastManager.getInstance().resultSetCasting(rs);
        if (list != null) {
            StringBuilder innerSQL = new StringBuilder();
            for (Map bean : list) {
                String beanInnerSQL = innerSQL(bean);
                if (innerSQL.length() == 0 && beanInnerSQL != null) {
                    innerSQL.append(beanInnerSQL);
                } else if (beanInnerSQL != null) {
                    innerSQL.append(" union all ").append(beanInnerSQL);
                }
            }
            String outerSQL = "SELECT A.id, A.status, A.title, A.related_to, A.tbl_name, LM.username created_by, A.created_ts, A.activity_id, A.smileys,created_bys, A.smiley_count, A.comment_count, LM.person_id, A.priority,A.file_url FROM \n"
                    + "( \n" + innerSQL + "\n) as A\n"
                    + "inner join login_master LM on(LM.id = A.created_by)";
            System.out.println("+++++++++++++outerSQL" + outerSQL);
            Statement stmt1 = con.createStatement();
            ResultSet rs1 = stmt1.executeQuery(outerSQL);
            List<Map> activityList = ObjectCastManager.getInstance().resultSetCasting(rs1);
            for (Map activityMap : activityList) {
                List<Map> commentList = fetchComments(con, (String) activityMap.get("activity_id"), "3");
                activityMap.put("commentList", commentList);
            }
            response.put("activityList", activityList);

            return activityList;
        }
        return null;
    }

    private String innerSQL(Map bean) {
        if ("note".equals(bean.get("table"))) {
            return "\nSELECT id, status, note title,  related_to, 'note' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys, '" + bean.get("created_bys") + "' created_bys, " + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,file_url FROM note where id = " + bean.get("table_pk");
        } else if ("Task".equals(bean.get("table"))) {
            return "\nSELECT task_id id, 'NEW' status, task_name title, related_to, 'Task' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count, priority as priority,file_url FROM Task where task_id = " + bean.get("table_pk");
        } else if ("event".equals(bean.get("table"))) {
            return "\nSELECT id, 'NEW' status,title,  related_to, 'event' as tbl_name, created_by, created_ts,  " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count, '' as priority,file_url FROM event where id = " + bean.get("table_pk");
        } else if ("opportunity".equals(bean.get("table"))) {
            return "\nSELECT id, status, name title, contact related_to, 'opportunity' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,file_url  FROM opportunity where id = " + bean.get("table_pk");
        } else if ("person_contact".equals(bean.get("table"))) {
            return "\nSELECT person_id id, status, concat(first_name,' ', last_name) title, '' related_to, 'person_contact' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,'' file_url  FROM person_contact where person_id = " + bean.get("table_pk");
        } else if ("company_contact".equals(bean.get("table"))) {
            return "\nSELECT company_id id, status, company_name title, '' related_to, 'company_contact' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,'' file_url  FROM company_contact where company_id = " + bean.get("table_pk");
        } else if ("trust_contact".equals(bean.get("table"))) {
            return "\nSELECT trust_id id, status, trust_name title, '' related_to, 'trust_contact' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,'' file_url  FROM trust_contact where trust_id = " + bean.get("table_pk");
        } else if ("file".equals(bean.get("table"))) {
            return "\nSELECT f_id id, '' status, discreption title,  related_to, 'file' as tbl_name, created_by, created_ts, " + bean.get("activity_id") + " activity_id, '" + bean.get("smileys") + "' smileys,  '" + bean.get("created_bys") + "' created_bys," + bean.get("smiley_count") + " smiley_count, " + bean.get("comment_count") + " comment_count,'' as priority,'' file_url  FROM file where f_id = " + bean.get("table_pk");
        }
        return null;
    }

    public void deleteActivities(HashMap request, HashMap response) {
        String id = (String) request.get("activity_id");
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String innerSql = "UPDATE save_activity SET active = 'N' WHERE id = " + id + ";";
            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
//            preparedStatement.setString(1, id);
            int i = preparedStatement.executeUpdate();
            response.put("message", "You have deleted a  Activity. Now you have received these Activity.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(ActivityRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ActivityRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
