/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.aws.lambda.repository;

import demo.aws.lambda.beans.RecentActivityBean;
import demo.aws.lambda.constant.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TOSHIBA R830
 */
public class LikeCommentRepository {

    private LikeCommentRepository() {
    }

    private static LikeCommentRepository instance;

    public static LikeCommentRepository getInstance() {
        if (instance == null) {
            instance = new LikeCommentRepository();
        }
        return instance;
    }

    public void updateSmiley(RecentActivityBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);

            String selectSQL = " SELECT * FROM `post_likes` LC where `activity_id` =" + bean.getActivity_id() + " and `created_by` = " + bean.getCreated_by();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);

            if (rs.next()) {
                String id = rs.getString("id");
                String like = rs.getString("like");
                if (like != null && !like.isEmpty()) {
                    String updateSQL = " UPDATE  `post_likes` SET  `like`=? where id=?";
                    PreparedStatement preparedStatement = con.prepareStatement(updateSQL);
                    preparedStatement.setString(1, null);
                    preparedStatement.setString(2, id);
                    preparedStatement.executeUpdate();
                } else {
                    String updateSQL = " UPDATE  `post_likes` SET  `like`=? where id=?";
                    PreparedStatement preparedStatement = con.prepareStatement(updateSQL);
                    preparedStatement.setString(1, bean.getSmileys());
                    preparedStatement.setString(2, id);
                    preparedStatement.executeUpdate();
                }

            } else {
                String emailSQL = " INSERT INTO `post_likes`(`like`, `activity_id`, `created_by`, `created_ts`)"
                        + " VALUES (?, ?, ?, ?);";
                PreparedStatement preparedStatement = con.prepareStatement(emailSQL);
                preparedStatement.setString(1, "&#128077");
                preparedStatement.setString(2, bean.getActivity_id());
                preparedStatement.setString(3, bean.getCreated_by());
                preparedStatement.setString(4, bean.getCreated_ts());
                preparedStatement.executeUpdate();

            }

            response.put("message", "You have assigned smiley at activity.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void updateCommentSmiley(RecentActivityBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String selectSQL = " SELECT * FROM `post_likes` where `activity_id` =" + bean.getActivity_id() + " and `comment_id` = " + bean.getComment_id() + " and `created_by` = " + bean.getCreated_by();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            if (rs.next()) {
                String id = rs.getString("id");
                String updateSQL = " UPDATE  `post_likes` SET  `like`= ? where id= ?";
                PreparedStatement preparedStatement = con.prepareStatement(updateSQL);
                preparedStatement.setString(1, bean.getSmileys());
                preparedStatement.setString(2, id);
                preparedStatement.executeUpdate();
            } else {
                String emailSQL = " INSERT INTO `post_likes`(`like`, `activity_id`, `comment_id`, `created_by`, `created_ts`)"
                        + " VALUES (?, ?, ?, ?, ?, ?);";
                PreparedStatement preparedStatement = con.prepareStatement(emailSQL);
                preparedStatement.setString(1, "&#128077");
                preparedStatement.setString(2, bean.getActivity_id());
                preparedStatement.setString(3, bean.getComment_id());
                preparedStatement.setString(4, bean.getCreated_by());
                preparedStatement.setString(5, bean.getCreated_ts());
                preparedStatement.executeUpdate();
            }
            response.put("message", "You have assigned smiley at activity.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void newComment(RecentActivityBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String emailSQL = " INSERT INTO `like_comment`(`comment`, `activity_id`,`created_by`, `created_ts`)"
                    + " VALUES (?, ?, ?, ?);";
            PreparedStatement preparedStatement = con.prepareStatement(emailSQL);
            preparedStatement.setString(1, bean.getComment());
            preparedStatement.setString(2, bean.getActivity_id());
            preparedStatement.setString(3, bean.getCreated_by());
            preparedStatement.setString(4, bean.getCreated_ts());
            preparedStatement.executeUpdate();
            List<Map> fetchComments = ActivityRepository.getInstance().fetchComments(con,bean.getActivity_id(),"1");
            response.put("commentList", fetchComments);
            response.put("message", "You have comment at activity.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
