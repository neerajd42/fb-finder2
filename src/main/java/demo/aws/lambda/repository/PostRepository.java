package demo.aws.lambda.repository;

import demo.aws.lambda.beans.ActivityBean;
import demo.aws.lambda.beans.NoteBean;
import demo.aws.lambda.components.ObjectCastManager;
import demo.aws.lambda.constant.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PostRepository {

    private static PostRepository instance;

    public static PostRepository getInstance() {
        if (instance == null) {
            instance = new PostRepository();
        }
        return instance;
    }

    public void newPost(NoteBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String innerSql = "INSERT INTO `user_posts`\n"
                    + "(`title`,\n"
                    + "`user_post`,\n"
                    + "`created_ts`,\n"
                    + "`related_to`,\n"
                    + "`created_by`,\n"
                    + "`file_url`,\n"
                    + "`active`)\n"
                    + "VALUES\n"
                    + "(?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "'Y');";
            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, bean.getTitle());
            preparedStatement.setString(2, bean.getUser_post());
            preparedStatement.setString(3, bean.getCreated_ts());
            preparedStatement.setString(4, bean.getRelated_to());
            preparedStatement.setString(5, bean.getCreated_by());
            preparedStatement.setString(6, bean.getFile_url());
            int i = preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                String table_pk = generatedKeys.getString(1);
                ActivityBean activityBean = new ActivityBean(bean.getCreated_by(), bean.getCreated_ts(), "user_post", table_pk);

                ActivityRepository.getInstance().newActivity(con, activityBean, response);
                fetchPostbyId(table_pk, response, con);
            }
            response.put("message", "You have created a new note.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void fetchPosts(HashMap request, HashMap response) {
        Connection con = null;
        List<Map> noteList = null;
        try {
            con = DBConnection.getConnection(true);
            String sql = " SELECT  distinct(N.id), N.title,N.file_url, N.user_post, N.created_ts,N.created_by as user_id,N.related_to, LM.username as created_by,SA.id activity_id,count(PL.like) as likes FROM user_posts N\n"
                    + "INNER JOIN login_master LM on (N.created_by = LM.id)\n"
                    + "inner join save_activity SA on (N.id = SA.table_pk and SA.table= 'user_post')\n"
                    + "left JOIN post_likes PL on (PL.activity_id = SA.id)\n"
                    + "group by SA.id  "
                    + "order by  N.id desc  ";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            noteList = ObjectCastManager.getInstance().resultSetCasting(rs);
            for (Map activityMap : noteList) {
                List<Map> commentList = fetchComments(con, (String) activityMap.get("activity_id"), "3");
                activityMap.put("commentList", commentList);
            }
            response.put("user_posts", noteList);
            response.put("message", "You have requested for tasks. Now you have these tasks.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("user_posts", noteList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void fetchPostbyId(String id, HashMap response, Connection con) {
        List<Map> noteList = null;
        try {
            String sql = " SELECT  distinct(N.id), N.title, N.file_url,N.user_post, N.created_ts,N.related_to,N.created_by as user_id, LM.username as created_by,SA.id activity_id FROM user_posts N\n"
                    + "INNER JOIN login_master LM on (N.created_by = LM.id)\n"
                    + "inner join save_activity SA on (N.id = SA.table_pk and SA.table= 'user_post')\n"
                    + "where N.id =" + id;
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            noteList = ObjectCastManager.getInstance().resultSetCasting(rs);
            response.put("user_posts", noteList);
        } catch (SQLException ex) {
            response.put("user_posts", noteList);
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
public List<Map> fetchComments(Connection con, String activity_id, String limit) throws SQLException {
        String sql = "  select LC.id, LC.comment, LC.like, LC.created_by, LC.created_ts, LM.username username from like_comment LC\n"
                + " inner join login_master LM on(LM.id = LC.created_by)\n"
                + " where LC.comment <> ''";
        if (activity_id != null && !activity_id.isEmpty()) {
            sql += " and LC.activity_id = " + activity_id;
        }
        sql += "  order by LC.id desc ";
//        if (limit != null) {
//            sql += " limit " + limit;
//        }
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        List<Map> list = ObjectCastManager.getInstance().resultSetCasting(rs);
        return list;
    }
    public void updatepost(NoteBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String innerSql = "UPDATE `note` SET\n"
                    + "`title` = ?,\n"
                    + "`note` = ?,\n"
                    + "`created_ts` = ?,\n"
                    + "`related_to` = ?,\n"
                    + "`created_by` = ?\n"
                    //                    + "`workspace_id` = ?,\n"
                    //                    + "`active` = 'Y'\n"
                    + "WHERE `id` = ?;";
            PreparedStatement preparedStatement = con.prepareStatement(innerSql);
            preparedStatement.setString(1, bean.getTitle());
            preparedStatement.setString(2, bean.getNote());
            preparedStatement.setString(3, bean.getCreated_ts());
            preparedStatement.setString(4, bean.getRelated_to());
            preparedStatement.setString(5, bean.getCreated_by());
//            preparedStatement.setString(6, bean.getWorkspace_id());
            preparedStatement.setString(6, bean.getId());
            preparedStatement.executeUpdate();
//           int x = Integer.parseInt(bean.getId());
//                ActivityBean activityBean = new ActivityBean(bean.getCreated_by(), bean.getCreated_ts(), "note", bean.getId());
//                activityBean.setGroup_id(bean.getGroup_id());
//                activityBean.setEmail(bean.getEmail());
//                ActivityRepository.getInstance().newActivity(con, activityBean, response
//                );
            response.put("message", "You have updated a note.");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void deletepost(HashMap request, HashMap response) {
        String ids = (String) request.get("note_id");
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String innerSql = "DELETE FROM note WHERE id in (" + ids + ");";
            PreparedStatement preparedStatement = con.prepareStatement(innerSql, Statement.RETURN_GENERATED_KEYS);
            int i = preparedStatement.executeUpdate();
//            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
//            if (generatedKeys.next()) {
//                ActivityBean activityBean = new ActivityBean(bean.getCreated_by(), bean.getCreated_ts(), "note", generatedKeys.getString(1));
//                activityBean.setGroup_id(bean.getGroup_id());
//                activityBean.setEmail(bean.getEmail());
//                ActivityRepository.getInstance().newActivity(con, activityBean, response);
//            }
            response.put("message", "You have deleted a  Note(s).");
            response.put("status", 200);
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
