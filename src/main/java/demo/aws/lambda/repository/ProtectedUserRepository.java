package demo.aws.lambda.repository;

import demo.aws.lambda.beans.LoginBean;
import demo.aws.lambda.constant.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProtectedUserRepository {

    private static ProtectedUserRepository instance;

    public static ProtectedUserRepository getInstance() {
        if (instance == null) {
            instance = new ProtectedUserRepository();
        }
        return instance;
    }

    public void saveRegister(LoginBean bean, HashMap response) {
        Connection con = null;
        try {
            con = DBConnection.getConnection(true);
            String selectSQL = " SELECT * FROM `login_master` where `email_id` ='" + bean.getEmail_id() + "'";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(selectSQL);
            if (rs.next()) {
                response.put("message", "This Email is already register");
                response.put("status", 200);
            } else {
                String sql = "INSERT INTO `login_master`\n"
                        + "(`username`,\n"
                        + "`password`,\n"
                        + "`email_id`,\n"
                        + "`created_ts`,\n"
                        + "`active`,\n"
                        + "`created_by`,\n"
                        + "`user_link`)\n"
                        + "VALUES\n"
                        + "(?,?,?,?,'Y',?,?); ";
                try (PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                    preparedStatement.setString(1, bean.getUsername());
                    preparedStatement.setString(2, bean.getPassword());
                    preparedStatement.setString(3, bean.getEmail_id());
                    preparedStatement.setString(4, bean.getCreated_ts());
                    preparedStatement.setString(5, "API");
                    preparedStatement.setString(6, bean.getUsername());
                    preparedStatement.executeUpdate();

                }
                response.put("message", "User Register Successfully");
                response.put("status", 200);
            }
        } catch (SQLException ex) {
            response.put("message", "SQL Server Error." + ex);
            response.put("status", 500);
            Logger.getLogger(PostRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProtectedUserRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
